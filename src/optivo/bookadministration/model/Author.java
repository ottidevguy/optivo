package optivo.bookadministration.model;

public class Author {

	
    protected String email;
	protected String lastname;
	protected String firstname;
	
	
	//Default
	public Author() {
		super();
	}
	
	public Author(String email, String firstname, String lastname) {
		super();
		this.email = email;
		this.lastname = lastname;
		this.firstname = firstname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String toString() {
		return "Author [email=" + email + ", lastname=" + lastname
				+ ", firstname=" + firstname + "]\n";
	}

	

}
