package optivo.bookadministration.model;

public class Book implements Comparable<Book> {

	

	protected String title;
	protected String description;
	protected String isbn;

	String authors;

	public Book(Long id, String title, String description, String author,
			String isbn) {
		super();
		this.title = title;
		this.description = description;
		this.isbn = isbn;
		this.authors = author;
	}

	public Book() {
	}

	public String getAuthors() {
		return authors;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getIsbn() {
		return isbn;
	}

	@Override
	public int compareTo(Book o) {
		if (o.getTitle() == null && this.getTitle() == null) {
			return 0;
		}
		if (this.getTitle() == null) {
			return 1;
		}
		if (o.getTitle() == null) {
			return -1;
		}
		return this.getTitle().compareTo(o.getTitle());
	}

	@Override
	public String toString() {
		return "Book [title=" + "'" + title + "'" + ", description=" + "'" + description
				+ "'" + ", isbn=" + "'" + isbn + "'" + ", authors=" + "'" + authors + "'" + "]\n";
	}
}
