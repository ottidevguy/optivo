package optivo.bookadministration.model;

public class Magazine implements Comparable<Magazine> {

	protected String title;
	protected String authors;
	protected String releaseDate;
	protected String isbn;

	public Magazine(String title, String authors, String releaseDate,
			String isbn) {
		super();
		this.title = title;
		this.authors = authors;
		this.releaseDate = releaseDate;
		this.isbn = isbn;
	}

	public String getAuthors() {
		return authors;
	}

	public String getTitle() {
		return title;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public String getIsbn() {
		return isbn;
	}

	@Override
	public int compareTo(Magazine o) {
		 if (o.getTitle() == null && this.getTitle() == null) {
		      return 0;
		    }
		    if (this.getTitle() == null) {
		      return 1;
		    }
		    if (o.getTitle() == null) {
		      return -1;
		    }
		    return this.getTitle().compareTo(o.getTitle());
		  }

	@Override
	public String toString() {
		return "Magazine [title=" + title + ", authors=" + authors
				+ ", releaseDate=" + releaseDate + ", isbn=" + isbn + "]\n";
	}
	}


