package optivo.bookadministration.connector;

public class OptivoBookAdmin {

	public static void main(String[] args) {

		BookAdminConnector connector = new BookAdminConnector();
		connector.showAllData();
		connector.printBooksSorted();
		connector.printMagazinesSorted();
	}

}
