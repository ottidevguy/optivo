package optivo.bookadministration.connector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import optivo.bookadministration.model.Author;
import optivo.bookadministration.model.Book;
import optivo.bookadministration.model.Magazine;
import optivo.bookadministration.service.AuthorService;
import optivo.bookadministration.service.AuthorServiceImpl;
import optivo.bookadministration.service.BookService;
import optivo.bookadministration.service.BookServiceImpl;
import optivo.bookadministration.service.MagazineService;
import optivo.bookadministration.service.MagazineServiceImpl;

public class BookAdminConnector {

	BookService bookservice;
	MagazineService magazineService;
	AuthorService authorService;

	
	public BookAdminConnector(){
		bookservice = BookServiceImpl.getInstance();
		magazineService = MagazineServiceImpl.getInstance();
		authorService = AuthorServiceImpl.getInstance();
	}
	
	public void showAllData() {
		// Alle Authoren
		for (Author author : authorService.getAllAuthors()) {
			System.out.println(author);
		}
		// Alle Bücher
		for (Book book : bookservice.getAllBooks()) {
			System.out.println(book);
		}
		// Alle Magazine
		for (Magazine magazine : magazineService.getAllMagazines()) {
			System.out.println(magazine);
		}
	}

	public void printMagazinesSorted() {
		List<Magazine> allMagazines = new ArrayList<Magazine>(
				magazineService.getAllMagazines());
		Collections.sort(allMagazines);
		for (Magazine magazine : allMagazines) {
			System.out.println(magazine);
		}
	}

	public void printBooksSorted() {
		List<Book> allBooks = new ArrayList<Book>(bookservice.getAllBooks());
		Collections.sort(allBooks);
		for (Book book : allBooks) {
			System.out.println(book);
		}
	}

	public Set<Book> findBookByAuthor(Author author) {
		Set<Book> books = new HashSet<Book>();
		if (null != author)
			books = bookservice.getBooksForAuthor(author);
		return books;
	}

	public Set<Magazine> findMagazineByAuthor(Author author) {
		Set<Magazine> magazines = new HashSet<Magazine>();
		if (null != author)
			magazines = magazineService.getMagazinesForAuthor(author);
		return magazines;
	}

	public Set<Book> findByBookIsbn(Long isbn) {
		Set<Book> books = new HashSet<Book>();
		if (null != isbn)
			books = bookservice.getByISBN(isbn.toString());
		return books;
	}

	public Set<Magazine> findMagazineByIsbn(Long isbn) {
		Set<Magazine> magazines = new HashSet<Magazine>();
		if (null != isbn)
			magazines = magazineService.getByISBN(isbn.toString());
		return magazines;
	}

}
