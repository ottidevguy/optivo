package optivo.bookadministration.service;

import java.util.Collection;
import java.util.Set;

import optivo.bookadministration.model.Author;
import optivo.bookadministration.model.Book;

public interface BookService {
	
	Collection<Book> getAllBooks();

	Set<Book> getBooksForAuthor(Author author);

	Set<Book> getByISBN(String isbnId);

}
