package optivo.bookadministration.service;

import java.util.Collection;

import optivo.bookadministration.model.Author;

public interface AuthorService {

	
	public Collection<Author> getAllAuthors();

}
