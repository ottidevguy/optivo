package optivo.bookadministration.service;

import java.util.Collection;
import java.util.Set;

import optivo.bookadministration.model.Author;
import optivo.bookadministration.model.Magazine;

public interface MagazineService {
	
	public Collection<Magazine> getAllMagazines();

	public Set<Magazine> getMagazinesForAuthor(Author author);

	public Set<Magazine> getByISBN(String isbnId);
}
