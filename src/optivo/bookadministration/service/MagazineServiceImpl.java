package optivo.bookadministration.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import optivo.bookadministration.model.Author;
import optivo.bookadministration.model.Magazine;
import optivo.bookadministration.util.CSVReader;

public class MagazineServiceImpl implements MagazineService {

	private static MagazineServiceImpl magazineservice = null;

	protected MagazineServiceImpl() {
		super();
	}

	public static MagazineServiceImpl getInstance() {
		if (magazineservice == null) {
			magazineservice = new MagazineServiceImpl();
		}
		return magazineservice;
	}

	public Collection<Magazine> getAllMagazines() {
		return readMagzinesFromCSV();
	}

	public Collection<Magazine> readMagzinesFromCSV() {

		CSVReader csvRader = new CSVReader(
				"/home/otti/Temp/data/zeitschriften.csv");
		List<Map<Integer, String>> magazineObjects = csvRader
				.getConvertedObjects();
		// attrbutes here : Titel, ISBN, Author, Erscheinung
		List<Magazine> magazines = new ArrayList<Magazine>();
		for (Map<Integer, String> attributesOfMagazine : magazineObjects) {
			String title = attributesOfMagazine.get(0);
			String isbn = attributesOfMagazine.get(1);
			String author = attributesOfMagazine.get(2);
			String release = attributesOfMagazine.get(3);
			Magazine magazine = new Magazine(title, author, release, isbn);
			magazines.add(magazine);
		}
		return magazines;
	};

	public Set<Magazine> getByISBN(String isbnId) {
		Collection<Magazine> magazines = readMagzinesFromCSV();
		Set<Magazine> result = new HashSet<Magazine>();
		for (Magazine magazine : magazines) {
			String isbn = magazine.getIsbn();
			// auch nett hier : authorsAsString.split(Pattern.quote(","));
			if (isbn.toLowerCase().equals(isbnId.toLowerCase())) {
				result.add(magazine);
			}
		}
		return result;
	}

	public Set<Magazine> getMagazinesForAuthor(Author author) {
		Collection<Magazine> magazines = readMagzinesFromCSV();
		Set<Magazine> result = new HashSet<Magazine>();
		for (Magazine magazine : magazines) {
			String authorsAsString = magazine.getAuthors();
			// auch nett hier : authorsAsString.split(Pattern.quote(","));
			if (authorsAsString.toLowerCase().contains(
					author.getEmail().toLowerCase())) {
				result.add(magazine);
			}
		}
		return result;
	}
}
