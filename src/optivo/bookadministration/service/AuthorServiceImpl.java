package optivo.bookadministration.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import optivo.bookadministration.model.Author;
import optivo.bookadministration.util.CSVReader;

public class AuthorServiceImpl implements AuthorService{

	private static AuthorServiceImpl authorservice = null;

	protected AuthorServiceImpl() {
		super();
	}

	public static AuthorServiceImpl getInstance() {
		if (authorservice == null) {
			authorservice = new AuthorServiceImpl();
		}
		return authorservice;
	}
	
	public Collection<Author> getAllAuthors() {
		return readAuthorsFromCSV();
	}

	private Collection<Author> readAuthorsFromCSV() {

		CSVReader csvRader = new CSVReader("/home/otti/Temp/data/autoren.csv");
		List<Map<Integer, String>> authoren = csvRader.getConvertedObjects();
		
		// attrbutes here : email, vorname, nachname
		List<Author> authors = new ArrayList<Author>();
		for (Map<Integer, String> attributesOfAuthor : authoren) {
			String email = attributesOfAuthor.get(0);
			String frontname = attributesOfAuthor.get(1);
			String backname = attributesOfAuthor.get(2);
			Author author = new Author(email, frontname, backname);
			authors.add(author);
		}
		return authors;
	};

}
