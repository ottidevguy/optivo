package optivo.bookadministration.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import optivo.bookadministration.model.Author;
import optivo.bookadministration.model.Book;
import optivo.bookadministration.util.CSVReader;

public class BookServiceImpl implements BookService {

	private static BookServiceImpl bookservice = null;

	protected BookServiceImpl() {
		super();
	}

	public static BookServiceImpl getInstance() {
		if (bookservice == null) {
			bookservice = new BookServiceImpl();
		}
		return bookservice;
	}

	public Collection<Book> getAllBooks() {
		return readBooksFromCSV();
	}

	private Collection<Book> readBooksFromCSV() {

		CSVReader csvRader = new CSVReader("/home/otti/Temp/data/buecher.csv");
		List<Map<Integer, String>> bookObjects = csvRader.getConvertedObjects();
		// attrbutes here : Titel, ISBN, Authoren, Description
		List<Book> books = new ArrayList<Book>();
		for (Map<Integer, String> attributesOfBook : bookObjects) {
			// int attirbuteCount = attributesOfBook.size();
			String title = attributesOfBook.get(0);
			String isbn = attributesOfBook.get(1);
			String author = attributesOfBook.get(2);
			String description = attributesOfBook.get(3);
			Book book = new Book(null, title, description, author, isbn);
			// TODO SET AUTHORS
			books.add(book);
		}
		return books;
	};

	public Set<Book> getBooksForAuthor(Author author) {

		Collection<Book> books = readBooksFromCSV();
		Set<Book> result = new HashSet<Book>();
		for (Book book : books) {
			String authorsAsString = book.getAuthors();
			// auch nett hier : authorsAsString.split(Pattern.quote(","));
			if (authorsAsString.toLowerCase().contains(
					author.getEmail().toLowerCase())) {
				result.add(book);
			}
		}
		return result;
	}

	public Set<Book> getByISBN(String isbnId) {
		Collection<Book> books = readBooksFromCSV();
		Set<Book> result = new HashSet<Book>();
		for (Book book : books) {
			String isbn = book.getIsbn();
			// auch nett hier : authorsAsString.split(Pattern.quote(","));
			if (isbn.toLowerCase().equals(isbnId.toLowerCase())) {
				result.add(book);
			}
		}
		return result;
	}

}
