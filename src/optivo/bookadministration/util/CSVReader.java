package optivo.bookadministration.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CSVReader {

	private String filename;

	public CSVReader(String filename) {
		this.filename = filename;
	}

	public List<Map<Integer, String>> getConvertedObjects() {

		Map<Integer, String> attributesMap = new HashMap<Integer, String>();
		String csvFile = filename;
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ";";
		List<Map<Integer, String>> convertedObjects = new ArrayList<Map<Integer, String>>();
		try {
			br = new BufferedReader(new FileReader(csvFile));
			br.readLine();
			while ((line = br.readLine()) != null) {
				// split the lins in attributes
				String[] attributes = line.split(cvsSplitBy);
				attributesMap = new HashMap<Integer, String>();
				for (int i = 0; i < attributes.length; i++) {
					attributesMap.put(i, attributes[i]);
				}
				convertedObjects.add(attributesMap);

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return convertedObjects;
	}

}
