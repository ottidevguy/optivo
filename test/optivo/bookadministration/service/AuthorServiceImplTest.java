package optivo.bookadministration.service;

import java.util.Collection;

import junit.framework.TestCase;
import optivo.bookadministration.model.Author;

import org.junit.Test;

public class AuthorServiceImplTest extends TestCase {

	AuthorService authorService = AuthorServiceImpl.getInstance();

	@Test
	public void allAuthorsTest() {
		Collection<Author> authors = authorService.getAllAuthors();
		assertEquals(6, authors.size());
	}
}
