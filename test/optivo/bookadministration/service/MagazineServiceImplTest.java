package optivo.bookadministration.service;

import java.util.Collection;
import java.util.Set;

import junit.framework.TestCase;
import optivo.bookadministration.model.Author;
import optivo.bookadministration.model.Magazine;

import org.junit.Test;

public class MagazineServiceImplTest extends TestCase {

	MagazineService magazineService = MagazineServiceImpl.getInstance();

	@Test
	public void allMagazinesTest() {
		Collection<Magazine> magazines = magazineService.getAllMagazines();
		assertEquals(6, magazines.size());
	}

	public void magazinesForAuthorTest() {
		Author author = new Author("pr-walter@optivo.de", "Paul", "Walter");
		Set<Magazine> magazines = magazineService.getMagazinesForAuthor(author);
		assertEquals(3, magazines.size());
	}

	public void booksForISBNTest(String isbnId) {

		Set<Magazine> magazines = magazineService.getByISBN("2145-8548-3325");
		assertEquals(1, magazines.size());
	}

}
