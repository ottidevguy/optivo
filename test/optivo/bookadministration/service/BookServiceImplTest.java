package optivo.bookadministration.service;

import java.util.Collection;
import java.util.Set;

import junit.framework.TestCase;
import optivo.bookadministration.model.Author;
import optivo.bookadministration.model.Book;

import org.junit.Test;

public class BookServiceImplTest extends TestCase {

	BookService bookService = BookServiceImpl.getInstance();

	@Test
	public void allAuthorsTest() {
		Collection<Book> books = bookService.getAllBooks();
		assertEquals(8, books.size());
	}

	public void booksForAuthorTest() {
		Author author = new Author("pr-walter@optivo.de", "Paul", "Walter");
		Set<Book> books = bookService.getBooksForAuthor(author);
		assertEquals(3, books.size());
	}

	public void booksForISBNTest(String isbnId) {

		Set<Book> books = bookService.getByISBN("2145-8548-3325");
		assertEquals(1, books.size());
	}

}
